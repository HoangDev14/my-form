import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app2.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // template form
  //topics = ['Angular', 'React', 'Vue'];

  // reactive form
    registrationForm = new FormGroup({
    userName: new FormControl('userName'),
    password: new FormControl('')
  });
}
